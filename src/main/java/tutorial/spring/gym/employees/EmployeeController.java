package tutorial.spring.gym.employees;

import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employees")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class EmployeeController {

    EmployeeManagementService employeeManagementService;

    @PostMapping
    ResponseEntity<Void> addEmployee(@RequestBody @Valid EmployeeData data) {
        final var employeeId = employeeManagementService.createEmployee(data);

        return ResponseEntity
                .created(URI.create("/employees/".concat(String.valueOf(employeeId))))
                .build();
    }
}
