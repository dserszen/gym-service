package tutorial.spring.gym.employees;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmployeeData {

    @NotBlank(message = "name is required")
    String name;

    @NotBlank(message = "surname is required")
    String surname;

    @DecimalMin(value = "1", message = "salary minimum is 1")
    @DecimalMax(value = "10000", message = "salary maximum is 10000")
    BigDecimal salary;
}
