package tutorial.spring.gym.employees;

import org.springframework.data.jpa.repository.JpaRepository;

interface EmployeeDao extends JpaRepository<EmployeeEntity, Long> {
}
