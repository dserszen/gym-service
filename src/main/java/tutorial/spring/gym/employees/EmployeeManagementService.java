package tutorial.spring.gym.employees;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class EmployeeManagementService {

    EmployeeDao employeeDao;

    @Transactional
    public Long createEmployee(EmployeeData data) {
        log.info(
                "Saving employee with name {} surname {} and salary {}",
                data.getName(), data.getSurname(), data.getSalary()
        );

        return employeeDao.save(
                new EmployeeEntity(null, data.getName(), data.getSurname(), data.getSalary())
        ).getId();
    }
}
